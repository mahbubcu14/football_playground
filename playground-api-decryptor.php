


<?php

function playground_shortcodes($atts ){
  $a = shortcode_atts( array(
        'col' => '0',
        'fullpage'=>"no"

    ), $atts );



$addi = 6;
if ($a['col']=='1'){
    $addi=7;
}elseif ($a['col']=='2'){
    $addi=8;
}elseif ($a['col']=='3'){
    $addi=9;
}

$isfullpage = $a['fullpage'];
//$onPage = get_on_page($a['col'],$addi);
$total_pack=get_tables($a['col'],$addi,$isfullpage);
//    if ($a['onpage']==="yes"){
//        return $onPage;
//    }else{
//        return $total_pack;
//    }
//
//
    return $total_pack;
}





add_shortcode( 'playground', 'playground_shortcodes' );

?>

<?php function get_tables($col,$addi,$isfull)
{
    if ($isfull == "yes") {
        global $wpdb;
        $sql_query = "select*from " . $wpdb->prefix . "play order by match_time";
        $all_match = $wpdb->get_results($sql_query);
        ?>
        <!doctype html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport"
                  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

            <!--Bootstarp CSS-->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
            <title>Document</title>


            <style>
                label {
                    color: #3d923d;
                    font-weight: bolder;
                    margin-top: 20px;
                }

                .table .thead-dark th {
                    text-align: center;
                }

                .table td, .table th {
                    text-align: center;
                    padding: .5rem!important;
                }

            </style>
        </head>
        <body>
        <div class="container-fluid">
            <?php if (!empty($all_match)){ ?>
            <div class="table-responsive-sm">
            <table style=" box-shadow: 2px 2px 6px 1px #d9e4e3;" class="table table-bordered table-striped">
                <thead>
                <tr class="table-primary">
                    <th scope="col"
                        colspan="<?php echo $addi; ?>"><?php echo date('d F,Y', strtotime($all_match[0]->match_time)); ?> </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Time</th>
                    <th>Home</th>
                    <th>Scores</th>
                    <th>Away</th>
                    <th>Comment</th>
                    <th>Price</th>
                    <?php
                    if ($col == '1') {
                        echo "<th>ext1</th>";
                    } elseif ($col == '2') {
                        echo "<th>ext1</th>
                      <th>ext2</th>";
                    } elseif ($col == '3') {
                        echo "<th>ext1</th>
                      <th>ext2</th>
                      <th>ext3</th>";
                    }
                    ?>
                </tr>
                <?php } ?>

                <?php
                $isToday = date('dF', strtotime($all_match[0]->match_time));
                $Firstleague = $all_match->league;
                foreach ($all_match

                as $match){
                $getDate = date('dF', strtotime($match->match_time));
                if ($getDate == $isToday){
//            if ($Firstleague!==$match->league){
//                echo '<tr><td colspan="'.$addi.'"><h4 style="text-align: center">'.$match->league.'</h4></td></tr>';
//                $Firstleague=$match->league;
//            }
                    ?>
                    <tr>
                        <td colspan="<?php echo $addi; ?>"><h4
                                    style="text-align: center"><?php echo $match->league; ?></h4></td>
                    </tr>
                    <tr>
                        <td><?php echo date('h:iA', strtotime($match->match_time)); ?></td>


                        <td>

                            <span <?php if (($match->check1) == 1) {
                                echo 'style="background: yellow"';
                            } ?>><?php echo $match->team1; ?></span>
                        </td>
                        <td>
                            <div style="display: inline-block">
                                <?php echo $match->score1; ?>-
                                <?php echo $match->score2; ?>
                            </div>


                        </td>
                        <td>

                            <span <?php if (($match->check2) == 1) {
                                echo 'style="background: yellow"';
                            } ?>><?php echo $match->team2; ?></span>
                        </td>
                        <td><?php if (($match->status) == 1) {
                                echo '<span style="background: red">(hot)</span>';
                            } ?><?php echo $match->comment; ?></td>
                        <?php

                        echo '<td>' . $match->price . '</td>';

                        ?>

                        <?php
                        if ($col == '1') {
                            echo "<th>" . $match->ext1 . "</th>";
                        } elseif ($col == '2') {
                            echo "<th>" . $match->ext1 . "</th>
                      <th>" . $match->ext2 . "</th>";
                        } elseif ($col == '3') {
                            echo "<th>" . $match->ext1 . "</th>
                      <th>" . $match->ext2 . "</th>
                      <th>" . $match->ext3 . "</th>";
                        }
                        ?>

                    </tr>

                <?php }else{
                $isToday = $getDate;
                ?>
                </tbody>
            </table></div>
        <div class="table-responsive-sm">
            <table style="box-shadow: 2px 2px 6px 1px #d9e4e3;" class="table table-bordered table-striped">
                <thead>
                <tr class="table-primary">
                    <th scope="col"
                        colspan="<?php echo $addi; ?>"><?php echo date('d F,Y', strtotime($match->match_time)); ?> </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Time</th>
                    <th>Home</th>
                    <th>Scores</th>
                    <th>Away</th>
                    <th>Comment</th>
                    <th>Price</th>

                    <?php
                    if ($col == '1') {
                        echo "<th>ext1</th>";
                    } elseif ($col == '2') {
                        echo "<th>ext1</th>
                      <th>ext2</th>";
                    } elseif ($col == '3') {
                        echo "<th>ext1</th>
                      <th>ext2</th>
                      <th>ext3</th>";
                    }
                    ?>
                </tr>
                <tr>
                    <td colspan="<?php echo $addi; ?>"><h4
                                style="text-align: center"><?php echo $match->league; ?></h4></td>
                </tr>
                <tr>
                    <td><?php echo date('h:iA', strtotime($match->match_time)); ?></td>


                    <td>

                        <span <?php if (($match->check1) == 1) {
                            echo 'style="background: yellow"';
                        } ?>><?php echo $match->team1; ?></span>
                    </td>
                    <td>
                        <div style="display: inline-block">
                            <?php echo $match->score1; ?> -
                            <?php echo $match->score2; ?>
                        </div>


                    </td>
                    <td>

                        <span <?php if (($match->check2) == 1) {
                            echo 'style="background: yellow"';
                        } ?>><?php echo $match->team2; ?></span>
                    </td>
                    <td><?php if (($match->status) == 1) {
                            echo '<span style="background: red">(Hot)</span>';
                        } else {
                            echo '';
                        } ?><?php echo $match->comment; ?></td>
                    <?php

                    echo '<td>' . $match->price . '</td>';

                    ?>

                    <?php
                    if ($col == '1') {
                        echo "<th>" . $match->ext1 . "</th>";
                    } elseif ($col == '2') {
                        echo "<th>" . $match->ext1 . "</th>
                      <th>" . $match->ext2 . "</th>";
                    } elseif ($col == '3') {
                        echo "<th>" . $match->ext1 . "</th>
                      <th>" . $match->ext2 . "</th>
                      <th>" . $match->ext3 . "</th>";
                    }
                    ?>
                </tr>
                <?php } ?>





                <?php } ?>
                <?php if (!empty($all_match)){ ?>
                </tbody>
            </table>
        <?php } ?>
        </div>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

        </body>
        </html>

    <?php
    } else {


        global $wpdb;
        $sql_query = "select*from " . $wpdb->prefix . "play order by match_time";
        $all_match = $wpdb->get_results($sql_query);
        ?>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <style>
            label {
                color: #3d923d;
                font-weight: bolder;
                margin-top: 20px;
            }

            .table .thead-dark th {
                text-align: center;
            }

            .table td, .table th {
                text-align: center;
                padding: .5rem!important;
                font-size: 50%;
            }

        </style>
        <div class="container-fluid">
            <?php if (!empty($all_match)){ ?>
        <div class="table-responsive-sm">
            <table style=" box-shadow: 2px 2px 6px 1px #d9e4e3;min-width: 600px" class="table table-bordered table-striped">
                <thead>
                <tr class="table-primary">
                    <th scope="col"
                        colspan="<?php echo $addi; ?>"><?php echo date('d F,Y', strtotime($all_match[0]->match_time)); ?> </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Time</th>
                    <th>Home</th>
                    <th>Scores</th>
                    <th>Away</th>
                    <th>Comment</th>
                    <th>Price</th>
                    <?php
                    if ($col == '1') {
                        echo "<th>ext1</th>";
                    } elseif ($col == '2') {
                        echo "<th>ext1</th>
                      <th>ext2</th>";
                    } elseif ($col == '3') {
                        echo "<th>ext1</th>
                      <th>ext2</th>
                      <th>ext3</th>";
                    }
                    ?>
                </tr>
                <?php } ?>

                <?php
                $isToday = date('dF', strtotime($all_match[0]->match_time));
                $Firstleague = $all_match->league;
                foreach ($all_match

                as $match){
                $getDate = date('dF', strtotime($match->match_time));
                if ($getDate == $isToday){
//            if ($Firstleague!==$match->league){
//                echo '<tr><td colspan="'.$addi.'"><h4 style="text-align: center">'.$match->league.'</h4></td></tr>';
//                $Firstleague=$match->league;
//            }
                    ?>
                    <tr>
                        <td colspan="<?php echo $addi; ?>"><h4
                                    style="text-align: center;font-size: 18px"><?php echo $match->league; ?></h4></td>
                    </tr>
                    <tr>
                        <td><?php echo date('h:iA', strtotime($match->match_time)); ?></td>


                        <td>

                            <span <?php if (($match->check1) == 1) {
                                echo 'style="background: yellow"';
                            } ?>><?php echo $match->team1; ?></span>
                        </td>
                        <td>
                            <div style="display: inline-block">
                                <?php echo $match->score1; ?>-
                                <?php echo $match->score2; ?>
                            </div>


                        </td>
                        <td>

                            <span <?php if (($match->check2) == 1) {
                                echo 'style="background: yellow"';
                            } ?>><?php echo $match->team2; ?></span>
                        </td>
                        <td><?php if (($match->status) == 1) {
                                echo '<span style="background: red">(hot)</span>';
                            } ?><?php echo $match->comment; ?></td>
                        <?php

                        echo '<td>' . $match->price . '</td>';

                        ?>

                        <?php
                        if ($col == '1') {
                            echo "<th>" . $match->ext1 . "</th>";
                        } elseif ($col == '2') {
                            echo "<th>" . $match->ext1 . "</th>
                      <th>" . $match->ext2 . "</th>";
                        } elseif ($col == '3') {
                            echo "<th>" . $match->ext1 . "</th>
                      <th>" . $match->ext2 . "</th>
                      <th>" . $match->ext3 . "</th>";
                        }
                        ?>

                    </tr>

                <?php }else{
                $isToday = $getDate;
                ?>
                </tbody>
            </table>
            </div>
            <div class="table-responsive-sm">
            <table style="box-shadow: 2px 2px 6px 1px #d9e4e3; min-width: 600px" class="table table-bordered table-striped ">
                <thead>
                <tr class="table-primary">
                    <th scope="col"
                        colspan="<?php echo $addi; ?>"><?php echo date('d F,Y', strtotime($match->match_time)); ?> </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Time</th>
                    <th>Home</th>
                    <th>Scores</th>
                    <th>Away</th>
                    <th>Comment</th>
                    <th>Price</th>

                    <?php
                    if ($col == '1') {
                        echo "<th>ext1</th>";
                    } elseif ($col == '2') {
                        echo "<th>ext1</th>
                      <th>ext2</th>";
                    } elseif ($col == '3') {
                        echo "<th>ext1</th>
                      <th>ext2</th>
                      <th>ext3</th>";
                    }
                    ?>
                </tr>
        <tr>
            <td colspan="<?php echo $addi; ?>"><h4
                        style="text-align: center; font-size: 18px"><?php echo $match->league; ?></h4></td>
        </tr>
                <tr>
                    <td><?php echo date('h:iA', strtotime($match->match_time)); ?></td>


                    <td>

                        <span <?php if (($match->check1) == 1) {
                            echo 'style="background: yellow"';
                        } ?>><?php echo $match->team1; ?></span>
                    </td>
                    <td>
                        <div style="display: inline-block">
                            <?php echo $match->score1; ?> -
                            <?php echo $match->score2; ?>
                        </div>


                    </td>
                    <td>

                        <span <?php if (($match->check2) == 1) {
                            echo 'style="background: yellow"';
                        } ?>><?php echo $match->team2; ?></span>
                    </td>
                    <td><?php if (($match->status) == 1) {
                            echo '<span style="background: red">(Hot)</span>';
                        } else {
                            echo '';
                        } ?><?php echo $match->comment; ?></td>
                    <?php

                    echo '<td>' . $match->price . '</td>';

                    ?>

                    <?php
                    if ($col == '1') {
                        echo "<th>" . $match->ext1 . "</th>";
                    } elseif ($col == '2') {
                        echo "<th>" . $match->ext1 . "</th>
                      <th>" . $match->ext2 . "</th>";
                    } elseif ($col == '3') {
                        echo "<th>" . $match->ext1 . "</th>
                      <th>" . $match->ext2 . "</th>
                      <th>" . $match->ext3 . "</th>";
                    }
                    ?>
                </tr>
                <?php } ?>





                <?php } ?>
                <?php if (!empty($all_match)){ ?>
                </tbody>
            </table>
        <?php } ?>
        </div>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>


    <?php }
}


