<?php

/**
*Plugin Name: Football Playground
*Plugin URI: http://minionslab.infinitethirst.com
*Author: Mahbubur Rahman
*Description: Managing Football Score
*Author URI: http://fiverr.com/mahbubfrr
*Version: 0.0.1
*License: GPLv2
*/

if (! defined('ABSPATH')) {
	exit;
}

include_once( plugin_dir_path( __FILE__ ) . 'playground-dashboard.php' );
include_once(plugin_dir_path(__FILE__).'playground-api-decryptor.php');

add_action( 'admin_menu', 'playground_menu' );
function playground_menu() {
  // add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
  add_menu_page( 'Playground Dashboard', 'Football Playground', 'manage_options', 'playground', 'playground_dashboard', 'dashicons-screenoptions', 90 );
}

function playground_admin_enqueue_scripts(){
	global $pagenow;
	if ($pagenow=='admin.php') {

        wp_register_script('prefix_bootstraps', '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js');
        wp_enqueue_script('prefix_bootstraps');

        // CSS
        wp_register_style('prefix_bootstraps', '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css');
        wp_enqueue_style('prefix_bootstraps');
        wp_register_style('prefix_bootstrapsf', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
        wp_enqueue_style('prefix_bootstrapsf');
    // JS
        wp_register_script('prefix_moment', 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js');
        wp_enqueue_script('prefix_moment');
//        wp_register_script('prefix_momentl', 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/ru.js');
//        wp_enqueue_script('prefix_momentl');

    wp_register_script('prefix_bootstrap', 'https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js');
    wp_enqueue_script('prefix_bootstrap');


    // CSS
    wp_register_style('prefix_bootstrap', 'https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css');
    wp_enqueue_style('prefix_bootstrap');

    wp_enqueue_script('playground-jquery',plugins_url('js/playground-jquery.js',__FILE__));
    wp_enqueue_script('playground-clip',plugins_url('js/clip.min.js',__FILE__));

    wp_enqueue_script('jquery');
    wp_enqueue_script( 'jquery-ui-datepicker', array( 'jquery' ) );

    wp_register_style('jquery-ui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css');
    wp_enqueue_style( 'jquery-ui' );

	}
}

function myplugin_activate() {
    $buf = "<?php\n"
        . "/*\n"
        . " * Template Name: Playground\n"
        . " */\n"
        . "
        if (have_posts()):
  while (have_posts()) : the_post();
    the_content();
  endwhile;
endif;
        ?>\n";

    $handle = fopen( get_stylesheet_directory() . '/football_playground.php', 'w' );
    fwrite( $handle, $buf );
    fclose( $handle );
}
register_activation_hook( __FILE__, 'myplugin_activate' );

global $jal_db_version;
$jal_db_version = '1.0';

function jal_install() {
    global $wpdb;
    global $jal_db_version;

    $table_name = $wpdb->prefix . 'play';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        match_time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        league mediumtext NOT NULL,
        team1 mediumtext NOT NULL,
        check1 mediumint NOT NULL,
        score1 mediumint NOT NULL,
        team2 mediumtext NOT NULL,
        check2 mediumint NOT NULL,
        score2 mediumint NOT NULL,
        status mediumint NOT NULL,
        price mediumtext NOT NULL,
        comment mediumtext NULL,
        ext1 mediumtext NULL,
        ext2 mediumtext NULL,
        ext3 mediumtext NULL,
        PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );

    add_option( 'jal_db_version', $jal_db_version );
}
register_activation_hook( __FILE__, 'jal_install' );

add_action('admin_enqueue_scripts','playground_admin_enqueue_scripts');

