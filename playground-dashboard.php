<?php
function playground_dashboard(){
    global $wpdb;
    $sql_query = "select*from ".$wpdb->prefix."play order by match_time";
    $all_match = $wpdb->get_results($sql_query);
    ?>



<style>
    label{
        color: #3d923d;
        font-weight: bolder;
        margin-top: 20px;
    }

    .date{
        margin-left: 5px;
    }

    .form-control{
        margin-top: 10px;
    }

    /*score table*/

    .table .thead-dark th {
        text-align: center;
    }


    .table td, .table th{
        text-align: center;
    }

    .scoreText{
        width: 10%;
    }



    @media screen and (max-width: 700px) {
        .scoreText{
            font-size: 10px;
        }


    }
</style>


<!--score table-->
    <?php if (!empty($all_match)){ ?>
    <table style="margin-top: 20px; max-width: 98%;box-shadow: 2px 2px 6px 1px #d9e4e3;" class="table table-bordered table-striped table-hover">
    <thead>
    <tr class="table-primary">
        <th scope="col" colspan="10"><?php echo date('d F,Y',strtotime($all_match[0]->match_time)); ?> </th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th>Time</th>
        <th>Home</th>
        <th>Scores</th>
        <th>Away</th>
        <th>Comment</th>
        <th>Price</th>
        <th>ext1</th>
        <th>ext2</th>
        <th>ext3</th>
        <th>Action</th>
    </tr>
    <?php } ?>

    <?php
    $isToday=date('dF',strtotime($all_match[0]->match_time));
foreach ($all_match as $match){
    $getDate = date('dF',strtotime($match->match_time));
    if($getDate==$isToday){

    ?>
        <tr class="ford<?php echo $match->id;?>">
            <td><?php echo date('h:iA',strtotime($match->match_time)); ?></td>


            <td >
                <input
                        type="checkbox" id="c1<?php echo $match->id;?>"
                        value="" <?php if(($match->check1)==1){ echo "checked";} ?> >
                <span  <?php if(($match->check1)==1){ echo 'style="background: yellow"';}?>><?php echo $match->team1; ?></span>
            </td>
            <td>
                <div style="display: inline-block">
                    <input style="width: 40px;
                            background: none;
                            border: none;
                            box-shadow: none;
                            border-bottom: 1px solid;
" type="text" id="team1score<?php echo $match->id; ?>" value="<?php echo $match->score1; ?>"> -
                    <input style="width: 40px;
                        background: none;
                        border: none;
                        box-shadow: none;
                        border-bottom: 1px solid;
" type="text" id="team2score<?php echo $match->id; ?>" value="<?php echo $match->score2; ?>">
                </div>



            </td>
            <td>
                <input type="checkbox"
                       id="c2<?php echo $match->id;?>"
                       value="" <?php if(($match->check2)==1){ echo "checked";}?>>
                <span  <?php if(($match->check2)==1){ echo 'style="background: yellow"';}?>><?php echo $match->team2; ?></span>
            </td>
            <td><input type="checkbox" id="st<?php echo $match->id;?>" value="" <?php if(($match->status)==1){ echo "checked";}else{echo "";}?>><?php if(($match->status)==1){ echo '<span style="background: red">(Hot)</span>';}else{echo '';}?><input style="
    font-size: 12px;
    height: 42px;" type="text" id="comments<?php echo $match->id;?>" value="<?php echo $match->comment;?>"/></td>
            <?php

            echo '<td><input style="
    font-size: 12px;
    height: 42px;" type="text" id="price'.$match->id.'" value="'.$match->price.'"/></td>';

            ?>


            <td><?php echo $match->ext1;?></td>
            <td><?php echo $match->ext2;?></td>
            <td><?php echo $match->ext3;?></td>
            <td class="scoreText"><button id  = "s<?php echo $match->id; ?>" class="btn btn-success upall">Update</button> <span id="m<?php echo $match->id;?>" class="delt dashicons dashicons-no-alt"></span></td>
        </tr>

<?php }else{
        $isToday=$getDate;
        ?>
        </tbody>
        </table>
        <table style="margin-top: 20px; max-width: 98%;box-shadow: 2px 2px 6px 1px #d9e4e3;" class="table table-bordered table-striped table-hover">
            <thead>
            <tr class="table-primary">
                <th scope="col" colspan="10"><?php echo date('d F,Y',strtotime($match->match_time)); ?> </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th>Time</th>
                <th>Home</th>
                <th>Scores</th>
                <th>Away</th>
                <th>Comment</th>
                <th>Price</th>
                <th>ext1</th>
                <th>ext2</th>
                <th>ext3</th>
                <th>Action</th>
            </tr>
        <tr class="ford<?php echo $match->id;?>">
            <td><?php echo date('h:iA',strtotime($match->match_time)); ?></td>


            <td >
                <input
                        type="checkbox" id="c1<?php echo $match->id;?>"
                        value="" <?php if(($match->check1)==1){ echo "checked";} ?> >
                <span  <?php if(($match->check1)==1){ echo 'style="background: yellow"';}?>><?php echo $match->team1; ?></span>
            </td>
            <td>
                <div style="display: inline-block">
                    <input style="width: 40px;
                            background: none;
                            border: none;
                            box-shadow: none;
                            border-bottom: 1px solid;
" type="text" id="team1score<?php echo $match->id; ?>" value="<?php echo $match->score1; ?>"> -
                    <input style="width: 40px;
                        background: none;
                        border: none;
                        box-shadow: none;
                        border-bottom: 1px solid;
" type="text" id="team2score<?php echo $match->id; ?>" value="<?php echo $match->score2; ?>">
                </div>



            </td>
            <td>
                <input type="checkbox"
                       id="c2<?php echo $match->id;?>"
                       value="" <?php if(($match->check2)==1){ echo "checked";}?>>
                <span  <?php if(($match->check2)==1){ echo 'style="background: yellow"';}?>><?php echo $match->team2; ?></span>
            </td>
            <td><input type="checkbox" id="st<?php echo $match->id;?>" value="" <?php if(($match->status)==1){ echo "checked";}else{echo "";}?>><?php if(($match->status)==1){ echo '<span style="background: red">(Hot)</span>';}else{echo '';}?><input style="
    font-size: 12px;
    height: 42px;" type="text" id="comments<?php echo $match->id;?>" value="<?php echo $match->comment;?>"/></td>
            <?php

            echo '<td><input style="
    font-size: 12px;
    height: 42px;" type="text" id="price'.$match->id.'" value="'.$match->price.'"/></td>';

            ?>


            <td><?php echo $match->ext1;?></td>
            <td><?php echo $match->ext2;?></td>
            <td><?php echo $match->ext3;?></td>
            <td class="scoreText"><button id  = "s<?php echo $match->id; ?>" class="btn btn-success upall">Update</button> <span id="m<?php echo $match->id;?>" class="delt dashicons dashicons-no-alt"></span></td>
        </tr>
            <?php }?>





<?php } ?>
            <?php if (!empty($all_match)){ ?>
    </tbody>
        </table>
    <?php } ?>


    <div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;background: lightgrey"><img src="<?php echo plugin_dir_url( __FILE__ ) .'image/loader.gif';?>" width="64" height="64" /><br>Updating..</div>


<!-- Trigger the modal with a button -->
<div class="text-center">
    <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myModal"><span class="dashicons dashicons-plus"></span> Set New Match</button>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->




        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="container">



                    <form name="form" id="form" action="" >
                        <h1 style="    text-align: center;
    padding: 40px;
    color: #55b380;
    font-weight: bolder;
    font-family: serif;">Match Fixer</h1>

                        <!--date-->
                        <div class="form-row justify-content-center">
                            <div class="form-group col-md-6">
                                <label class="date" for="inputDate">DATE</label>
                                <div class="input-group date" id="datetimepicker1" data-target-input="nearest">

                                    <input style="margin-top: 0;" id="inputDate" class="form-control datetimepicker-input" data-target="#datetimepicker1"/>
                                    <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="validationCustom03">League Name</label>
                                <select class="form-control form-control-lg" name="league" id="validationCustom03" onchange="ChangecatList()" required>
                                    <option value="">Select league</option>
                                    <option value="Premier League">English Premier league</option>
                                    <option value="LALIGA">La-Liga</option>
                                </select>
                                <div class="invalid-feedback">
                                    Please select a league
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-6">
                                <label for="validationCustom04">Team 1 </label>
                                <select class="form-control form-control-lg" id="validationCustom04" name="team1" required></select>
                                <div class="invalid-feedback">
                                    Select first team
                                </div>
                            </div>



                            <div class="col-md-6">
                                <label for="validationCustom05">Team 2 </label>
                                <select class="form-control form-control-lg" id="validationCustom05" name="team2" required></select>
                                <div class="invalid-feedback">
                                    Select second team
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="price">Price</label>
                            <input type="text" class="form-control" id="price">
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="match-comment">Comments</label>
                                    <textarea class="form-control" id="match-comment" name="comment" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ext1">Additional 1</label>
                            <input type="text" class="form-control" id="ext1">
                        </div>
                        <div class="form-group">
                            <label for="ext2">Additional 2</label>
                            <input type="text" class="form-control" id="ext2">
                        </div>
                        <div class="form-group">
                            <label for="ext3">Additional 3</label>
                            <input type="text" class="form-control" id="ext3">
                        </div>




                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button id="setMatch" type="submit" class="btn btn-outline-primary">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>

    </div>
</div>








<!--custom js-->
<script>
    var catAndActs = {};

    catAndActs["Premier League"] = ["Arsenal","Aston Villa","Barnsley","Birmingham City","Blackburn Rovers","Blackpool","Bolton","Wanderers", "Bournemouth", "Bradford City","Brighton & Hove Albion","Burnley","Cardiff City","Charlton Athletic","Chelsea","Coventry City","Crystal Palace","Derby County","Everton","Fulham","Huddersfield Town","Hull City","Ipswich Town","Leeds United","Leicester City","Liverpool","Manchester City","Manchester United","Middlesbrough","Newcastle United","Norwich City","Nottingham Forest","Oldham Athletic","Portsmouth","Queens Park","Rangers","Sheffield United","Sheffield Wednesday","Southampton","Stoke City","Sunderland","Swansea City","Swindon Town","Tottenham Hotspur","Watford","West Bromwich","Albion","West Ham United","Wigan Athletic","Wimbledon","Wolverhampton","Wanderers"];

    catAndActs["LALIGA"] = ["Alaves","Ath Bilbao","Atl. Madrid","Barcelona","Betis","Celta Vigo","Eibar","Espanyol","Getafe","Girona","Huesca","Leganes","Levante","Rayo Vallecano","Real Madrid","Real Sociedad","Sevilla","Valencia","Valladolid","Villarreal"];

    function ChangecatList() {
        var catList = document.getElementById("validationCustom03");
        var actList = document.getElementById("validationCustom04");
        var actListone = document.getElementById("validationCustom05");
        var selCat = catList.options[catList.selectedIndex].value;
        while (actList.options.length || actListone.options.length) {
            actList.remove(0);
            actListone.remove(0);
        }
        var cats = catAndActs[selCat];
        var cats1 = catAndActs[selCat];
        if (cats) {
            var i;
            for (i = 0; i < cats.length; i++) {
                var cat = new Option(cats[i], i);
                actList.options.add(cat);
            }

        }

        if (cats1) {
            var i;
            for (i = 0; i < cats.length; i++) {
                var cat1 = new Option(cats1[i], i);
                actListone.options.add(cat1);
            }
        }
    }

</script>
    <script>
    
    			   jQuery(document).ready(function($) {

                       $("#datetimepicker1").datetimepicker();



                       $(".delt").on('click',function () {
                           var buttonId = this.id;
                           var matchId = buttonId.substring(1, buttonId.length);



                           console.log(matchId);
                           $("#wait").css("display", "block");

                           var data = {
                               "action": "my_action",
                               "id" : matchId,
                               "iam":"delt"
                           };

                           jQuery.post(ajaxurl, data, function(response) {
                               console.log(response);
                               $("#wait").css("display", "none");
                               // if (response=="delGo"){
                                   var delCla = ".ford"+matchId;
                                   $(delCla).hide("slow", function(){ $(this).remove(); })
                                   //$(delCla).remove();

                               // }else {
                               //     console.log(response);
                               //
                               // }
                           });



                       });



    			       $(".upall").on("click",function () {

    			           var getD = this.id;
    			           var matchId = getD.substring(1,getD.length);
    			           var team1 = "#team1score"+ matchId;
    			           var team2 = "#team2score" + matchId;
                           var c1 ="#c1" + matchId;
                           var c2 = "#c2"+matchId;
                           var cmnt="#comments"+matchId;
                           var price = "#price"+matchId;
                           var st = "#st"+matchId;
                           var status = 0;
                           var check1=0;
                           var check2=0;
                           var pr = $(price).val();
                           if($(c1).is(':checked')){
                               check1=1;
                           }else{
                               check1=0;
                           }
                           if($(c2).is(':checked')){
                               check2=1;
                           }else{
                               check2=0;
                           }

                           if($(st).is(':checked')){
                               status=1;
                           }else{
                               status=0;
                           }


                           var score1 = $(team1).val();
    			           var score2 = $(team2).val();
    			           var comment = $(cmnt).val();
    			           console.log(score1 + " " +score2);
                           $("#wait").css("display", "block");
    			           var data = {
                               "action": "my_action",
                               "score1" : score1,
                               "score2" : score2,
                               "iam":"updateall",
                               "id" : matchId,
                               "check1": check1,
                               "check2" :check2,
                               "status" : status,
                               "comment" : comment,
                               "price" : pr
                           };

                           jQuery.post(ajaxurl, data, function(response) {
                               console.log("second option"+ check2);
                               $("#wait").css("display", "none");
                               location.reload(); //Bug
                               if (response.search("scoreGo")===true){
                                   console.log("Score Done");
                                   location.reload();
                               }else {
                                   console.log(response);
                               }
                           });



                       });

    			       $(".commen").on('click',function () {
    			            var buttonId = this.id;
    			            var matchId = buttonId.substring(1, buttonId.length);
    			            var commentID = "#comments"+matchId;
                           var commentString = $(commentID).val();


    			            console.log(commentString);
                            $("#wait").css("display", "block");

                           var data = {
                               "action": "my_action",
                               "newComment" : commentString,
                               "id" : matchId,
                               "iam":"comment"
                           };

                           jQuery.post(ajaxurl, data, function(response) {
                               console.log(response);
                               $("#wait").css("display", "none");
                               if (response=="commentGo"){
                                   console.log("DOne");
                               }else {
                                   console.log(response);
                               }
                           });



                       });



                       $("#form").submit(function(e){

                    e.preventDefault();
                    var md = $("#inputDate").val();
                    var date = moment(md,"MM/DD/YYYY HH:mm a");
                    var matchdate = date.format("YYYY-MM-DD HH:mm:ss");
                    // console.log(matchdate);
                    var leagueName = $("#validationCustom03 option:selected").text();
                    var team1 = $("#validationCustom04 option:selected").text();
                    var team2 = $("#validationCustom05 option:selected").text();
                    var comment  = $("#match-comment").val();
                    var price = $("#price").val();
                    var ext1 = $("#ext1").val();
                    var ext2 = $("#ext2").val();
                    var ext3 = $("#ext3").val();
                    var data = {
					    "action": "my_action",
                        "matchDate" : matchdate,
                        "leagueName" : leagueName,
                        "team1" : team1,
                        "team2" : team2,
                        "comment" : comment,
                        "price" : price,
                        "ext1" : ext1,
                        "ext3" : ext3,
                        "ext2" : ext2,
                        "iam" : "modal"
					};

	  				jQuery.post(ajaxurl, data, function(response) {

                    //console.log(data);
	  				console.log(response)
                        $("#myModal").modal("toggle");//Bug
                        location.reload(); //Bug

                        if (response=="success"){
	  				       $("#myModal").modal("toggle");
	  				       location.reload();
                        }
					});
					});

					
				});

</script> <?php


}

add_action( 'wp_ajax_my_action', 'my_action' );


function my_action() {
    global $wpdb;
    $tname = $wpdb->prefix.'play'; //Table Name with Prefix

    if($_POST['iam']=='modal'){
        $matchDate  = $_POST['matchDate'];
        $league  = $_POST['leagueName'];
        $team1  = $_POST['team1'];
        $team2  = $_POST['team2'];
        $comment  = $_POST['comment'];
        $ext1  = $_POST['ext1'];
        $ext2  = $_POST['ext2'];
        $ext3  = $_POST['ext3'];
        $price  = $_POST['price'];
        $status = $wpdb->insert($tname,array(
                "match_time" => $matchDate,
                "league" => $league,
                "team1" =>$team1,
                "check1" => 0,
                "check2" =>0,
                "team2" => $team2,
                "price" => $price,
                "comment" => $comment,
                "score1" => 0,
                "score2" => 0,
                "status" => 0,
                "ext1" => $ext1,
                "ext2" => $ext2,
                "ext3" => $ext3
            )

        );


        if($status){
            echo "success";
        }else{
            echo $wpdb->last_error ;
        }



        wp_die(); // this is required to terminate immediately and return a proper response

    }else if ($_POST['iam']=='comment'){
        $comment = $_POST['newComment'];
        $id = $_POST['id'];
        $updated=$wpdb->update($tname, array('id'=>$id, 'comment'=>$comment), array('id'=>$id));
        if($updated){
            echo "commentGo";
        }else{
            echo $wpdb->last_error;
        }



    }else if ($_POST['iam'] == 'updateall'){
        $score1 = $_POST['score1'];
        $score2 = $_POST['score2'];
        $id = $_POST['id'];
        $price = $_POST['price'];
        $check1 = $_POST['check1'];
        $check2 = $_POST['check2'];
        $status = $_POST['status'];
        $comments = $_POST['comment'];


        $updates=$wpdb->update($tname, array(
            'id'=>$id,
            'score1'=>$score1,
            'score2'=>$score2,
            'check1' =>$check1,
            'check2'=>$check2,
            'price'=>$price,
            'status' =>$status,
            'comment' =>$comments


        ), array('id'=>$id));
        if($updates){
            echo "scoreGo";
        }else{
            echo $wpdb->last_error;
        }


    }else if($_POST['iam']=='delt'){
        $id = $_POST['id'];
        $del = $wpdb->delete( $tname, array( 'id' => $id ) );
        if ($del){
            echo "delGo";
        }else{
            echo $wpdb->last_error;
        }

    }

    wp_die();

}

